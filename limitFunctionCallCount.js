function limitFunctionCallCount(cb, n) {
    // Returns a function that invokes `cb`.
    // The returned function only allows `cb` to be invoked `n` times.
    // Returns null if cb can't be returned

    let count = n;
    let limitFunction = function (...args) {
        if (count >= 1) {
            cb(args);
            count -= 1;
        }
        else {
            return null;
        }
    };

    return limitFunction;
}

module.exports = limitFunctionCallCount;
