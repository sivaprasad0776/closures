let counterFactory = require('../counterFactory');
let counterFactoryObject = counterFactory();
let counter = 10;

//checking the increment functionality
let incrementedResult = counterFactoryObject.increment(counter);
if (incrementedResult == undefined) {
    console.log('Please enter a valid number');
}
else {
    console.log(`Incremented value of ${counter} is ${incrementedResult}`);
}

//checking the decrement functionality
let decrementedResult = counterFactoryObject.decrement(counter);
if (decrementedResult == undefined) {
    console.log('Please enter a valid number');
}
else {
    console.log(`Decremented value of ${counter} is ${decrementedResult}`);
}