let cacheFunction = require('../cacheFunction');

function cb(args){
    return args.length;
}

let cachedFunction = cacheFunction(cb);
let result;

result = cachedFunction();
console.log(result);

result= cachedFunction(1,2);
console.log(result);

result = cachedFunction(1);
console.log(result);

result = cachedFunction(1,2);
console.log(result);



