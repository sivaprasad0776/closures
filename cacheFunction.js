function cacheFunction(cb) {
    // Returns a function that invokes `cb`.
    // A cache (object) is kept in closure scope.
    // The cache keeps track of all arguments have been used to invoke this function.
    // If the returned function is invoked with arguments that it has already seen
    // then it returns the cached result and not invoke `cb` again.
    // `cb` is invoked only once for a given set of arguments.

    let cache = {};

    let cachedFunction = function (...args) {
        let cached = false;
        let result;
        for (const [key] of Object.entries(cache)) {
            if (key == args.toString()){
                cached = true;
                result = cache[key];
                break;
            }
            
        } 
        if(cached == false){
            result = cb(args);
            argsString = args.toString()
            cache[argsString] = result;
        }
        return result;   
    };

    return cachedFunction;
}

module.exports = cacheFunction;

