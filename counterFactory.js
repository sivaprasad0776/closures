function counterFactory() {
    // Returns an object that has two methods called `increment` and `decrement`.
    // `increment` increments a counter variable in closure scope and returns it.
    // `decrement` decrements the counter variable and returns it.
    
    return {
        increment(counter) {
            if (typeof counter != 'number') {
                return undefined;
            } else {
                counter += 1;
                return counter;
            }
        },
        decrement(counter) {
            if (typeof counter != 'number') {
                return undefined;
            } else {
                counter -= 1;
                return counter;
            }
        }
    }
}

module.exports = counterFactory;
